var express = require("express");
var router = express.Router();
var websocket = require("ws");

router.get("/", function(request, response, next){
	response.render("chat/index.ejs");
});

router.get("/index", function(request, response, next){
	response.redirect("/");
});

router.get("/login", function(request, response, next){

	response.render("landing/login");

});

router.post("/login", function(request, response, next){

	var session = request.session;

	session.user = request.body.user;

	response.redirect("/chat");

});

module.exports = router;
