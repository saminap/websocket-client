
var currentChannel = 0;
var allChannels = {};
var newMessageCount = {};
var username = "";
var ws;

$(document).ready(function(){

	//ws = new WebSocket("ws://85.23.230.181:8088");
	ws = new WebSocket("ws://localhost:8088");

	$("#chat").hide();
	$("#login").show();

	ws.onmessage = function(event){
		var data = JSON.parse(event.data);
		if(data.type == 1){
			if(data.status){
				$("#status").text("connected:D");
				for(var channel in data.channels){
					channel = data.channels[channel];
					$("#channels").append("<a class=\"channel-link\" id=\"" + channel.id + "\" href=\"#\">#" + channel.name + "</a><span class=\"unread-message-count\" id=\"unread-message-count-" + channel.id + "\">0</span><br />");
					$("#messages").append("<div class=\"messages-channel\" id=\"messages-channel-" + channel.id + "\"></div>");
					$("#messages-channel-" + channel.id).hide();
					allChannels[channel.id] = channel.name;
					newMessageCount[channel.id] = 0;
				}
				$("#login-screen").hide();
				$("#chat").show();
				$("#chat").height("100%");
				hideAllChannels();
				changeChannel(data.channels[0].id);
			}
			else {
				alert("Login failed");
			}
		}
		else if(data.type == 2){
			newMessage(data);
		}
		else if(data.type == 3){
			userJoinedToChannel(data);
		}
	};

	$(document).on('click', "a.channel-link", function() {
		changeChannel(this.id);
	});

	$("#logout").click(function(){
		ws.close();
		window.location.href="/";
	});

	$(document).keypress(function(e) {
		if(e.which == 13) {
			sendMessage();
		}
	});

	$("#submit").click(function() {
		sendMessage();
	});

	$("#login-form").submit(function(e){
		e.preventDefault();

		var loginInfo = { type: 1, username: this.username.value, password: this.password.value};
		ws.send(JSON.stringify(loginInfo));
		setUsername(this.username.value);
	});

});

function changeChannel(channel){
	$("#messages-channel-" + currentChannel).hide();
	$("#messages-channel-" + channel).show();
	$("#channel-name").text("#" + allChannels[channel]);
	$("#unread-message-count-" + channel).hide();
	newMessageCount[channel] = 0;
	currentChannel = channel;
}

function hideAllChannels(){
	for(var channel in allChannels){
		$("#messages-channel-" + allChannels[channel]).hide();
	}
}

function sendMessage(){
	var newmessage = $("#new-message").val();

	if(newmessage != ""){
		message = { type: 2, channel: currentChannel, message: newmessage};
		appendMessage(currentChannel, getMessageFormat(username, newmessage));
		$("#new-message").val("");
		ws.send(JSON.stringify(message));
		autoScrollMessages(currentChannel);
	}
}

function setUsername(name){
	$("#logged-username").text(name);
	username = name;
}

function newMessage(data){
	appendMessage(data.channel, getMessageFormat(data.from, data.message));
	notifyAboutMessage(data.channel, data.from, data.message);
	autoScrollMessages(data.channel);
}

function userJoinedToChannel(data){
	appendMessage(data.channel, "<div class=\"user-joined\">" + data.username + " joined to <span class=\"channel-name\">#" + allChannels[data.channel] + "</span></div>");
}

function appendMessage(channel, message){
	$("#messages-channel-" + channel).append(message);
}

function autoScrollMessages(channel){
	$("#messages-channel-" + channel).scrollTop($("#messages-channel-" + channel)[0].scrollHeight);
}

function getMessageFormat(sender, message){
	return "<div class=\"message\"><div class=\"username\">" + sender + "</div><div class=\"text\">" + message + "</div></div>";
}

function notifyAboutMessage(channel, sender, message){
	Push.create("New message on #" + allChannels[channel], {
		body: sender + ": " + message,
		onClick: function () {
			window.focus();
			this.close();
		},
	vibrate: [200, 100, 200, 100, 200, 100, 200]
	});

	if(currentChannel != channel){
		updateNewMessageCount(channel);
	}
}

function updateNewMessageCount(channel){
	newMessageCount[channel] = newMessageCount[channel] + 1;
	var block = $("#unread-message-count-" + channel)
	block.show();
	block.text("+" + newMessageCount[channel]);
}
