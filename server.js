var http = require('http');
var express = require('express');
var app = express();
var path = require('path');
var bodyparser = require('body-parser');
var session = require('express-session');

var $ = require('jquery');

var routes = require("./routes");
var port = 8081;

app.use(bodyparser.json()); // for parsing application/json
app.use(bodyparser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(session({secret: '1234567890QWERTY'}));
app.use('/', routes);
app.set('port', port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use('/css', express.static(path.join(__dirname, 'static/css')));
app.use('/js', express.static(path.join(__dirname, 'static/js')));

var server = http.createServer(app);
server.listen(port);
server.listen(port);
